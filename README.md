## 常用数据信息表
### phome_ecms_softdata 软件表 phome_ecms_news  新闻表 phome_enewszt 专题表

## 数据信息各表对应字段
### onclick   onclick   onclick   点击量 
### newstime  newstime  addtime   时间 
### title 				title 				ztname					标题
### titlepic				titlepic			ztimg					缩略图
### titleurl				titleurl			ztpath					url地址
### softsay				smalltext			intro					简介
### classid				classid				zcid					所属分类

## 关联表字段说明
## phome_ecms_softdata_data_1	软件下载地址表
### downpath 软件下载地址

## phome_enewsclass		栏目信息表
### classname 栏目名称		classpath 栏目url

## phome_enewsztinfo 		专题内容表
### ztid 专题id

## phome_enewsztclass		专题分类表
### classid 分类id		classname 分类名称

## phome_statistics_click_rank  信息访问量统计表
### statistics_type 统计类型	click_number 访问量

### 例:调取栏目id=1('1')的10条数据,以时间倒序('newstime DESC')
#### [e:loop={1,10,0,0,'','newstime DESC'}]
#### &lt;a href="<?=$bqsr['titleurl']?>" target="_blank"><?=$bqr['title']?></a>
#### [/e:loop]


### 例:调取栏目id=5和id=17('5,17')并且是1级推荐('isgood=1')的10条数据,以点击量倒序('onclick DESC')
### [e:loop={'5,17',10,0,0,'isgood=1','onclick DESC'}]
#### &lt;a href="<?=$bqsr['titleurl']?>" target="_blank"><?=$bqr['title']?></a>
#### [/e:loop]

### 例:调取栏目id=6并且是1级头条('firsttitle=1')的10条数据,以点击量倒序('onclick DESC')
#### [e:loop={6,10,0,0,'firsttitle=1','onclick DESC'}]
#### &lt;a href="<?=$bqsr['titleurl']?>" target="_blank"><?=$bqr['title']?></a>
#### [/e:loop]

### 例:调取新闻('news')点击量最高的10条数据,以点击量倒序('onclick DESC')
#### [e:loop={'news',10,18,0,'','onclick DESC'}]
#### &lt;a href="<?=$bqsr['titleurl']?>" target="_blank"><?=$bqr['title']?></a>
#### [/e:loop]

### 例:调取当前栏目('selfinfo')2级头条('firsttitle=1')的10条数据,以时间倒序('newstime DESC')
#### [e:loop={'selfinfo',10,0,0,'firsttitle=2','newstime DESC'}]
#### &lt;a href="<?=$bqsr['titleurl']?>" target="_blank"><?=$bqr['title']?></a>
#### [/e:loop]

## (注意:以下sql的栏目id必须是终极栏目id)
### 例:调取软件表(phome_ecms_softdata)栏目id=8和id=10的10条数据的常用信息(不包含下载地址)及所属分类的url和名称,以点击量倒序('onclick DESC')
#### [e:loop={"select a.filesize,a.onclick,a.language,a.id,a.title,a.softsay,a.newstime,a.banben,a.titlepic,a.titleurl,b.classpath,b.classname from [!db.pre!]ecms_softdata as a left join [!db.pre!]enewsclass as b on a.classid=b.classid where a.classid=8 or a.classid=10 order by a.onclick desc limit 10",10,24,0}]
#### &lt;a href="<?=$bqsr['titleurl']?>" target="_blank"><?=$bqr['title']?></a>
#### [/e:loop]

### 例:调取软件表(phome_ecms_softdata)栏目id=8和id=10的10条数据的常用信息(包含下载地址)及所属分类的url和名称,以点击量倒序('onclick DESC')
#### [e:loop={"select a.filesize,a.onclick,a.language,a.id,a.title,a.softsay,a.newstime,a.banben,a.titlepic,a.titleurl,b.classpath,b.classname,c.downpath from [!db.pre!]ecms_softdata as a left join [!db.pre!]enewsclass as b on a.classid=b.classid left join [!db.pre!]ecms_softdata_data_1 as c on a.id=c.id where a.classid=8 or a.classid=10 order by a.onclick desc limit 10",10,24,0}]
#### &lt;a href="<?=$bqsr['titleurl']?>" target="_blank"><?=$bqr['title']?></a>
#### [/e:loop]


### 例:调取软件2017年第23周(2017#23)访问量最高的10条数据,以点击量倒序('click_number DESC')
#### [e:loop={"select a.click_number,b.title,b.titleurl from [!db.pre!]statistics_click_rank as a left join [!db.pre!]ecms_softdata as b on a.url_id=b.id where a.url_type=1 and a.statistics_type=2 and a.time_interval='2017#23' order by a.click_number desc limit 10",10,24,0}]
#### &lt;a href="<?=$bqsr['titleurl']?>" target="_blank"><?=$bqr['title']?></a>
#### [/e:loop]

### 例:调取软件2017年访问量最高的10条数据,以点击量倒序('click_number DESC')
#### [e:loop={"select a.click_number,b.title,b.titleurl from [!db.pre!]statistics_click_rank as a left join [!db.pre!]ecms_softdata as b on a.url_id=b.id where a.url_type=1 and a.statistics_type=4 and a.time_interval='2017' order by a.click_number desc limit 10",10,24,0}]
#### &lt;a href="<?=$bqsr['titleurl']?>" target="_blank"><?=$bqr['title']?></a>
#### [/e:loop]